﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using eVision;
using System.Threading.Tasks;

namespace eVisionTests
{
    [TestClass]
    public class AccountTests 
    {
        [TestMethod]
        public void AccountInfo_RefreshAmount_Should_Return_Expected_Amount_Using_Mock()
        {
            //Arrange
            var accountId = 1;
            var expectedAmount = 1;
            //added a mock service using moq
            var accountMock = new Mock<IAccountService>();
            accountMock.Setup(m => m.GetAccountAmount(accountId)).Returns(expectedAmount).Verifiable();
            AccountInfo cu = new AccountInfo(accountId, accountMock.Object);
            //Act
            cu.RefreshAmount();
            //Assert
            Assert.AreEqual(expectedAmount, cu.Amount);
        }



        [TestMethod]
        public void AccountInfo_RefreshAmount_Should_Return_Expected_Amount_Using_Test_Class()
        {
            //Arrange
            var accountId = 1;
            var expectedAmount = 1;
            //added a mock service using moq
            AccountInfo cu = new AccountInfo(accountId, new AccountInfoTest());
            //Act
            cu.RefreshAmount();
            //Assert
            Assert.AreEqual(expectedAmount, cu.Amount);
        }



        //class implements IAccountService for testing
        class AccountInfoTest : IAccountService
        {
            public double GetAccountAmount(int accountId)
            {
                return accountId;
            }
        }


        //Task 2

        //calling the interface syncrhonously
        [TestMethod]
        public async Task AccountInfo_RefreshAmount_Should_Return_Expected_Amount_Synchronous()
        {

            //Arrange
            var accountId = 1;
            var expectedAmount = 1.0;
            //added a mock service using moq
            var accountMock = new Mock<IAccountServiceAsync>();
            //return a task result of expected amount.
            accountMock.Setup(x => x.GetAccountAmount(accountId)).Returns(() =>Task.FromResult(expectedAmount)); ;
            AccountInfoAsync cu = new AccountInfoAsync(accountId, accountMock.Object);
            //Act
            await cu.RefreshAmount();
            //Assert
            Assert.AreEqual(expectedAmount, cu.Amount);

        }

        //calling the interface asyncrhonously
        [TestMethod]
        public async Task AccountInfo_RefreshAmount_Should_Return_Expected_Amount_Asynchronous()
        {

            //Arrange
            var accountId = 1;
            var expectedAmount = 1.0;
            //added a mock service using moq
            var accountMock = new Mock<IAccountServiceAsync>();
            //return a task result of expected amount.
            accountMock.Setup(x => x.GetAccountAmount(accountId)).Returns(async() => {await Task.Yield(); return expectedAmount;});
            AccountInfoAsync cu = new AccountInfoAsync(accountId, accountMock.Object);
            //Act
            await cu.RefreshAmount();
            //Assert
            Assert.AreEqual(expectedAmount, cu.Amount);

        }

        //Exception if the remote service failed.
        [TestMethod]
        [ExpectedException(typeof(System.Exception))]
        public async Task AccountInfo_RefreshAmount_Should_Throw_Exception()
        {

            //Arrange
            var accountId = 1;
            //added a mock service using moq
            var accountMock = new Mock<IAccountServiceAsync>();
            //return a task result of expected amount.
            accountMock.Setup(x => x.GetAccountAmount(accountId)).Returns(async () => {
                await Task.Yield();
                throw new Exception();
            });
            AccountInfoAsync cu = new AccountInfoAsync(accountId, accountMock.Object);
            //Act
            await cu.RefreshAmount();
           //Exceptions show throw to pass the test.
        }


    }
}
