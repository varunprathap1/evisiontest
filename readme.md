Task 1
------------


Unit test that asserts the behaviour of RefreshAmount() method.

Solved using two ways

 .using a moq library to mock the interface.
 .created a test class that extends the IAccountService.


Task 2
------------

 .IAccountService changed for asynchronous operation. Awaited RefreshAmount method for concurrent calls.

 .Unit test are added for new IAccountServiceAsync interface.



Task 3
------------
Running the build.bat script will build, test and create nuget package.

1.Build Script

  .Build script is created in the root folder 'build.bat'.

  .Default path for msbuild.exe is defined as 'C:\Program Files (x86)\MSBuild\14.0\Bin' , Please change the path on bat file if another version of visual studio is using.

2.Test script

  .Default path for vstest.console.exe is defined as C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\CommonExtensions\Microsoft\TestWindow
, please change the path on bat file if another version of visual studio is using.

3.Nuget Package
 
  .Nuget.exe program is copied into the root folder, if available in the machine please update the path of nuget.exe on bat file.
  
------------------------------


  











