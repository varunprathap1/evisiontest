﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eVision
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("evisionTest"); 
        }
    }


    public class AccountInfo
    {
    private readonly int _accountId;
    private readonly IAccountService _accountService;
    public AccountInfo(int accountId, IAccountService accountService)
    {
        _accountId = accountId;
        _accountService = accountService;
    }
    public double Amount { get; private set; }
    public void RefreshAmount()
    {
        Amount = _accountService.GetAccountAmount(_accountId);
    }
    }

    public interface IAccountService
    {
        double GetAccountAmount(int accountId);
    }


    //Task 2
    public class AccountInfoAsync
    {
        private readonly int _accountId;
        private readonly IAccountServiceAsync _accountServiceAsync;
        public AccountInfoAsync(int accountId, IAccountServiceAsync accountService)
        {
            _accountId = accountId;
            _accountServiceAsync = accountService;
        }
        public double Amount { get; private set; }
        //create an async task to call a remote service asynchronously
        public async Task RefreshAmount()
        {
            //await call to interface.
            Amount = await _accountServiceAsync.GetAccountAmount(_accountId);
        }
    }

    //Interface
    public interface IAccountServiceAsync
    {
        Task<double> GetAccountAmount(int accountId);
    }




}
