REM set msBuildDir=C:\Program Files (x86)\MSBuild\14.0\Bin

set msBuildDir=C:\Program Files (x86)\MSBuild\14.0\Bin

call "%msBuildDir%\msbuild.exe" eVision.sln /t:Clean,Build 

set msBuildDir=

sleep 2

REM set msTestDir=C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\CommonExtensions\Microsoft\TestWindow

set msTestDir=C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\CommonExtensions\Microsoft\TestWindow

call "%msTestDir%\vstest.console.exe" eVisionTests\bin\Debug\eVisionTests.dll

set msTestDir=

sleep 2

nuget pack Package.nuspec

